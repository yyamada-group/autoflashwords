using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class JsonHandler
{
    // Saving destination
    // Needs to use persistentDataPath to keep data after quitting app
    static readonly string datapath = Application.persistentDataPath + "/flashworddata.json";
    //static string datapath = Application.dataPath + "/TestJson.json";


    // Reads and loads JSON file
    public static List<Word> LoadPlayerData()
    {
        Debug.Log($"path: {datapath}");

        string datastr;
        StreamReader reader;
        reader = new StreamReader(datapath);
        datastr = reader.ReadToEnd();
        reader.Close();
        ListSerialilzeWrap wrappedList = JsonUtility.FromJson<ListSerialilzeWrap>(datastr);
        return wrappedList.list.ToList<Word>();
    }

    // Saves JSON file
    public static void SavePlayerData(List<Word> list)
    {
        Debug.Log($"path: {datapath}");

        StreamWriter writer;
        // Converts data to JSON
        string jsonstr = JsonUtility.ToJson(new ListSerialilzeWrap(list));

        Debug.Log(jsonstr);

        // Writes JSON into file
        writer = new StreamWriter(datapath, false);
        writer.Write(jsonstr);
        writer.Flush();
        writer.Close();
    }
}
