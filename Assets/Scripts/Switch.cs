using UnityEngine;
using UnityEngine.UI;
public class Switch : MonoBehaviour
{
    [SerializeField] private RectTransform _handle;
    [SerializeField] private Toggle _toggle;
    [SerializeField] private Image _backgroundImage;
    private Color _backgroundOnColor = Color.green, _backgroundOffColor = Color.gray;

    void Start()
    {
        _backgroundImage.color = _backgroundOffColor;
    }

    public void ToggleChanged()
    {
        _handle.anchoredPosition *= -1.0f;
        if (_toggle.isOn)
            _backgroundImage.color = _backgroundOnColor;
        else
            _backgroundImage.color = _backgroundOffColor;
    }


    public void OnColorChangeButtonClicked(ThreeColors colors)
    {
        Debug.Log("switch change color");

        _backgroundOnColor = colors.Base;
        _backgroundOffColor = colors.Sub;

        _backgroundImage.color = _toggle.isOn ? _backgroundOnColor : _backgroundOffColor;
    }
}