using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Word
{
    public string _spell;
    public string _meaning;
    public string _level;
    public int _studiedNum = 0;

    public Word(string spell, string meaning)
    {
        _spell = spell;
        _meaning = meaning;
    }

    public Word(string spell, string meaning, string level) : this(spell, meaning)
    {
        this._level = level;
    }
}
