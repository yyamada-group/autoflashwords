using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class StartPauseToggle : MonoBehaviour
{
    [SerializeField] private Toggle _toggle;
    [SerializeField] private Sprite _playSprite;
    [SerializeField] private Sprite _pauseSprite;

    public void ToggleChanged()
    {
        _toggle.GetComponentInChildren<Image>().sprite = _toggle.isOn ? _pauseSprite : _playSprite;
        //_toggle.GetComponentInChildren<Image>().color = _toggle.isOn ? Color.red : Color.green;
    }
}
