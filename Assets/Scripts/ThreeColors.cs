using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ThreeColors
{
    public Color Base { get; set; }
    public Color Accent { get; set; }
    public Color Sub { get; set; }

    private Color[] _threeColors = new Color[3];
 

    private static float f256 = 256f;

    public ThreeColors(Color @base, Color accent, Color sub)
    {
        Base = @base;
        Accent = accent;
        Sub = sub;

        _threeColors[0] = @base;
        _threeColors[1] = accent;
        _threeColors[0] = sub;
    }

    public static ThreeColors third()
    {
        //light green,green,white
        ThreeColors colors = new(
        new(255f / f256, 208f / f256, 208f / f256)
        , new(156f / f256, 160f / f256, 197f / f256)
        , new(249f / f256, 241f / f256, 234f / f256)
        );
        return colors;
    }

    public static ThreeColors first()
    {
        //blue,white,yellow
        float[,] rgb = {
            { 0xb2, 0xc9, 0xba }, 
            { 0xfd, 0xeb, 0xde },
            { 0x88, 0x9d, 0x85 },
        };
        return threeColors(rgb);
    }

    public static ThreeColors second()
    {
        //pink,white,blue
        float[,] rgb = {
            { 0xc2, 0xda, 0xda },
            { 0xca, 0xae, 0x67 },
            { 0xf3, 0xf3, 0xed },
        };
        return threeColors(rgb);
    }

    public static ThreeColors threeColors(float[,] rgb)
    {
     
        ThreeColors colors = new(
          new(rgb[0, 0] / f256, rgb[0, 1] / f256, rgb[0, 2] / f256)
        , new(rgb[1, 0] / f256, rgb[1, 1] / f256, rgb[1, 2] / f256)
        , new(rgb[2, 0] / f256, rgb[2, 1] / f256, rgb[2, 2] / f256)
        );
        return colors;
    }


    public void ShuffleColors()
    {
        _threeColors = _threeColors.OrderBy(i => Guid.NewGuid()).ToArray();
        Base = _threeColors[0];
        Accent = _threeColors[1];
        Sub = _threeColors[2];
   
    }
}
