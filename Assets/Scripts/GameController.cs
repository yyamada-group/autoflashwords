using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using System.IO;

public class GameController : MonoBehaviour
{
    private float _fixedFlashIntervalTime = 0.7f;
    private float _variableFlashIntervalCoefficient = 0.13f;

    private readonly float[] _variableSpeedMinAndMax = { 0.1f, 0.5f };
    private readonly float[] _fixedSpeedMinAndMax = { 0.5f, 3.0f };
    private readonly int[] _flashSpeedSliderMinAndMax = { 1, 30 };

    private bool _fixedSpeedOn = false;
    [SerializeField] private GameObject MainPanel;
    [SerializeField] private Button FlashWordButton;
    [SerializeField] private Text NoText;
    [SerializeField] private Button ColorChangeButton;
    [SerializeField] private Text StudyResultText;
    [SerializeField] private Button MemorizedButton;
    [SerializeField] private Toggle MeaningToggle;
    [SerializeField] private Toggle PlayPauseToggle;
    [SerializeField] private Toggle FixedSpeedToggle;
    [SerializeField] private Toggle UncheckedOnlyToggle;
    [SerializeField] private Toggle FlipMeaningToggle;
    [SerializeField] private GameObject TogglesPanel;
    [SerializeField] private Slider WordJumpSlider;
    [SerializeField] private Slider SpeedControlSlider;
    [SerializeField] private Dropdown CategoryDropdown;
    [SerializeField] private Button CheckButton;
    [SerializeField] private Button ShuffleButton;
    [SerializeField] private Text DebugText;
    [SerializeField] private List<Text> ToggleSwitchTexts;

    private readonly List<Button> _speedChangeButtons = new();

    private float _flashTime = 0.0f;
    private bool _flashRunning = false;
    private bool _showMeaningOn = false;

    private List<Word> _currentWordList = new();
    private List<Word> _allWordList = new();
    private int _currentUncheckedWordListSize;
    private int _currentAllWordListSize;

    private int _currentWordIdx = 0;

    private readonly List<ThreeColors> _threeColorSets = new();
    private int _currentThreeColorSetIdx = 0;

    private float _timeFromStartToStop = 0;
    private int _studiedWordCount = 0;

    private bool _skipCheckedWords = false;

    private List<string> _levelList = new();

    private bool _showStudyResultWaiting = true;

    private readonly int[] _phases = { 1, 2 };
    private int _currentPhaseIdx = 0;

    private bool _flipped = false;

    private readonly int _flashWordFontSize = 60;
    private readonly int _flashMeaningFontSize = 45;

    private Color _checkColor = new(0.1f, 0.1f, 0.1f);
    private readonly float _uncheckedMarkAlpha = 0.1f;
    private readonly float _checkedMarkAlpha = 0.8f;
    private readonly string _checkMarkStr = "✓";

    // GUI drawing
    private Texture _texture;

    // progress bar color
    private Color _progressBarFrameColor = Color.gray;
    private Color _progressBarPhase1FillColor = Color.gray;
    private Color _progressBarPhase2FillColor = Color.white;

    // for debug
    private readonly bool _debugOn = false;

    void Awake()
    {
        LoadAndSaveInAwake();
    }

    private void LoadAndSaveInAwake()
    {
        if (_debugOn)
        {
            _allWordList = InitializeWords();
            JsonHandler.SavePlayerData(_allWordList);
        }

        try
        {
            //単語が増えた場合に、差分だけ増やすことができない
            _allWordList = JsonHandler.LoadPlayerData();
        }
        catch (FileNotFoundException e)
        {
            _allWordList = InitializeWords();
            JsonHandler.SavePlayerData(_allWordList);
            Debug.Log("File Not Found");
            Debug.Log(e.StackTrace);
        }
        catch (UnauthorizedAccessException e)
        {
            _allWordList = InitializeWords();
            JsonHandler.SavePlayerData(_allWordList);
            Debug.Log("Unauthorizaed Access");
            Debug.Log(e.StackTrace);
        }

        PrepareCurrentWords();

        // Called back when quitting app
        Application.quitting += () => JsonHandler.SavePlayerData(_allWordList);
    }

    private List<Word> InitializeWords()
    {
        //return WordGenerator.GenerateVerbs();
        return WordGenerator.GenerateAllWords();
    }


    // Start is called before the first frame update
    void Start()
    {
        InitializeColorSets();

        UpdateWordJumpSliderMaxValue();
        InitializeFlashSpeedSlider();
        InitializeDropdown();
        FilterCategory();
        ResetUiAndSetPhaseTo2();
        SetWordListSize(_currentWordList);
        UpdateNoTextUi();
        DrawTextureOnStart();
        OnColorChangeButtonClicked();
    }

    private void PrepareCurrentWords()
    {
        foreach (Word w in _allWordList)
        {
            _currentWordList.Add(w);
        }
        _levelList = _currentWordList.Select(w => w._level).Distinct().ToList();
    }

    // Update is called once per frame
    void Update()
    {
        if (_showStudyResultWaiting)
        {
            _showStudyResultWaiting = false;
            StudyResultText.text = GenerateTextForStudyResult();
            Debug.Log(StudyResultText.text);
        }
        else if (_flashRunning)
        {
            _timeFromStartToStop += Time.deltaTime;
            _flashTime += Time.deltaTime;
            if (IsUpdateTimeMet())
            {
                UpdateUi();
            }
        }
    }

    private string GenerateTextForStudyResult()
    {
        return $"勉強時間: {Math.Floor(_timeFromStartToStop / 60)}分 {Math.Floor(_timeFromStartToStop % 60)}秒\n勉強単語数: {_studiedWordCount}語";
    }

    private string GenerateTextForNoText()
    {
        return $"{this._currentWordIdx + 1} / {_currentAllWordListSize}({_currentUncheckedWordListSize})";
    }

    private void UpdateUi()
    {
        if (IsCurrentPhase1())
        {
            IncrementWordIdx();
        }

        Word currentWord = CurrentWord();
        string firstPhaseStr = _flipped ? currentWord._meaning : currentWord._spell;
        string secondPhaseStr = _flipped ? currentWord._spell : currentWord._meaning;
        int firstPhaseFontSize = _flipped ? _flashMeaningFontSize : _flashWordFontSize;
        int secondPhoseFontSize = _flipped ? _flashWordFontSize : _flashMeaningFontSize;

        Debug.Log($"phase: {_phases[_currentPhaseIdx]}, phaseIdx: {_currentPhaseIdx}");
        if (IsCurrentPhase1())
        {
            //phase 1
            _studiedWordCount += 1;
            _flashTime = 0;
            UpdateNoTextUi();
            UpdateWordUi(firstPhaseStr, firstPhaseFontSize);
            UpdateStudiedButtonUi();
        }
        else if (IsCurrentPhase2())
        {
            if (_showMeaningOn)
            {
                //phase 2
                _flashTime = 0;
                UpdateWordUi(secondPhaseStr, secondPhoseFontSize);
            }
        }
        MoveOnToNextPhase();
    }

    private bool IsCurrentPhase2()
    {
        return _phases[_currentPhaseIdx] == 2;
    }

    private bool IsCurrentPhase1()
    {
        return _phases[_currentPhaseIdx] == 1;
    }

    private void MoveOnToNextPhase()
    {
        _currentPhaseIdx = (_currentPhaseIdx + 1) % _phases.Length;
    }


    private bool IsUpdateTimeMet()
    {
        return _flashTime > (_fixedSpeedOn
                        ? _fixedFlashIntervalTime
                        : CalculateVariableWordTime()
                        );
    }

    private float CalculateVariableWordTime()
    {
        return _variableFlashIntervalCoefficient * CurrentWord()._spell.Length;
    }


    private void UpdateWordJumpSliderMaxValue()
    {
        WordJumpSlider.maxValue = _currentWordList.Count - 1;
    }

    private void InitializeDropdown()
    {
        CategoryDropdown.ClearOptions();
        CategoryDropdown.AddOptions(_levelList);

    }

    private void InitializeColorSets()
    {
        _threeColorSets.Add(ThreeColors.first());
        _threeColorSets.Add(ThreeColors.second());
        _threeColorSets.Add(ThreeColors.third());
    }

    private void SetUiColors()
    {
        var currentColor = _threeColorSets[_currentThreeColorSetIdx];

        MainPanel.GetComponent<Image>().color = currentColor.Base;
        NoText.color = currentColor.Sub;
        StudyResultText.color = currentColor.Sub;
        _checkColor = currentColor.Accent;

        MemorizedButton.GetComponentInChildren<Text>().color = currentColor.Accent;

        SetButtonColor(FlashWordButton);
        SetButtonColor(ColorChangeButton);
        SetButtonColor(CheckButton);
        SetButtonColor(ShuffleButton);
    }


    private void SetButtonColor(Button b)
    {
        b.GetComponent<Image>().color = _threeColorSets[_currentThreeColorSetIdx].Sub;
        b.GetComponentInChildren<Text>().color = _threeColorSets[_currentThreeColorSetIdx].Accent;
    }

    private void UpdateStudiedButtonUi()
    {
        Text checkText = MemorizedButton.GetComponentInChildren<Text>();
        bool studied = IsCurrentWordChecked();
        checkText.text = _checkMarkStr;
        _checkColor.a = studied ? _checkedMarkAlpha : _uncheckedMarkAlpha;
        checkText.color = _checkColor;

    }


    private void UpdateNoTextUi()
    {
        NoText.text = GenerateTextForNoText();
    }


    private void UpdateWordUi(string updatedText, int fontSize)
    {
        FlashWordButton.GetComponentInChildren<Text>().text = updatedText;
        FlashWordButton.GetComponentInChildren<Text>().fontSize = fontSize;
        FlashWordButton.GetComponentInChildren<Text>().alignment = TextAnchor.MiddleCenter;
    }

    private bool IsCurrentWordChecked()
    {
        return CurrentWord()._studiedNum == 1;
    }

    private void IncrementWordIdx()
    {
        IncrementWordIdxByOne();
        if (_skipCheckedWords)
        {
            while (IsCurrentWordChecked())
            {
                IncrementWordIdxByOne();
            }
        }
        WordJumpSlider.value = _currentWordIdx;
    }

    // When infinite loop can happen 
    // 1. when a word is checked -> OnWordChecked
    // 2. when a level is changed -> OnLevelChanged
    // 3. when shuffled -> OnShuffleClicked
    // 4. when skip checked word toggled -> OnUncheckedToggled


    private void IncrementWordIdxByOne()
    {
        _currentWordIdx = (_currentWordIdx + 1) % _currentWordList.Count;
    }

    private void DecrementWordIdx()
    {
        DecrementWordIdxByOne();
        if (_skipCheckedWords)
        {
            while (IsCurrentWordChecked())
            {
                DecrementWordIdxByOne();
            }
        }
        WordJumpSlider.value = _currentWordIdx;
    }

    private void DecrementWordIdxByOne()
    {
        int newIdx = _currentWordIdx - 1;
        _currentWordIdx = newIdx < 0 ? _currentWordList.Count - 1 : newIdx;
    }


    public void OnPlayPauseToggleTapped()
    {
        _flashRunning = PlayPauseToggle.isOn;
        if (!PlayPauseToggle.isOn)
        {
            _showStudyResultWaiting = true;
        }
        //Text ppText = PlayPauseToggle.GetComponentInChildren<Text>();
        //ppText.text = PlayPauseToggle.isOn ? "■" : "▶"; //◼︎
        //ppText.text = PlayPauseToggle.isOn ? "PAUSE" : "PLAY"; //◼︎
        //ppText.fontSize = PlayPauseToggle.isOn ? 100 : 150;
        _flashTime = 0;
    }


    public void OnColorChangeButtonClicked()
    {
        _currentThreeColorSetIdx = (_currentThreeColorSetIdx + 1) % _threeColorSets.Count;
        SetUiColors();
        foreach (Button b in _speedChangeButtons)
        {
            SetButtonColor(b);
        }

        ThreeColors colors = _threeColorSets[_currentThreeColorSetIdx];

        // toggle colors
        var toggleSwitches = GameObject.FindGameObjectsWithTag("ToggleSwitch");
        foreach (var toggleSwitch in toggleSwitches)
        {
            Switch sw = toggleSwitch.GetComponent<Switch>();
            sw.OnColorChangeButtonClicked(colors);
        }

        foreach (Text t in ToggleSwitchTexts)
        {
            t.color = colors.Base;
        }

        // dropdown colors
        CategoryDropdown.image.color = colors.Sub;
        var drowdownLabels = GameObject.FindGameObjectsWithTag("DropdownLabel");
        foreach (var dropdownLabel in drowdownLabels)
        {
            dropdownLabel.GetComponent<Text>().color = colors.Accent;
        }

        //slider colors
        foreach (var sliderBackground in GameObject.FindGameObjectsWithTag("SliderBackground"))
        {
            sliderBackground.GetComponent<Image>().color = colors.Base;
        }
        foreach (var sliderFill in GameObject.FindGameObjectsWithTag("SliderFill"))
        {
            sliderFill.GetComponent<Image>().color = colors.Accent;
        }
        foreach (var sliderHandle in GameObject.FindGameObjectsWithTag("SliderHandle"))
        {
            sliderHandle.GetComponent<Image>().color = colors.Sub;
        }

        // progress bar colors
        _progressBarFrameColor = colors.Accent;
        _progressBarPhase1FillColor = colors.Accent;
        _progressBarPhase2FillColor = colors.Sub;
    }

    public void OnCheckButtonClicked()
    {
        CheckFlagCurrentWord();
    }

    public void OnMemorizedButtonClicked()
    {
        CheckFlagCurrentWord();
    }

    private void CheckFlagCurrentWord()
    {
        int checkNum = (CurrentWord()._studiedNum + 1) % 2;
        CurrentWord()._studiedNum = checkNum;

        if (checkNum == 0)
        {
            _currentUncheckedWordListSize++;
        }
        else if (checkNum == 1)
        {
            _currentUncheckedWordListSize--;
        }
        UpdateNoTextUi();
        UpdateStudiedButtonUi();
    }

    private bool AreAllWordsChecked()
    {
        return _currentAllWordListSize == 0;
    }

    private Word CurrentWord()
    {
        return _currentWordList[_currentWordIdx];
    }


    private void SetWordListSize(List<Word> newWordList)
    {
        _currentUncheckedWordListSize = newWordList.Where(w => w._studiedNum == 0).Count();
        _currentAllWordListSize = newWordList.Count();
    }


    public void OnCategoryDropdownValueChanged()
    {
        FilterCategory();
        ResetUiAndSetPhaseTo2();
    }

    private void FilterCategory()
    {
        string levelStr = CategoryDropdown.options[CategoryDropdown.value].text;
        ResetCurrentWordIdxToZero();
        List<Word> filteredWords = _allWordList.Where(w => w._level == levelStr).ToList();
        UpdateCurrentWordList(new());

        foreach (Word w in filteredWords)
        {
            _currentWordList.Add(w);
        }
        SetWordListSize(_currentWordList);
        UpdateWordJumpSliderMaxValue();
    }


    public void OnUncheckedOnlyToggleTapped()
    {
        _skipCheckedWords = UncheckedOnlyToggle.isOn;
        SetWordListSize(_currentWordList);

        if (_skipCheckedWords)
        {
            if (IsCurrentWordChecked())
            {
                IncrementWordIdx();
            }
        }
        ResetUiAndSetPhaseTo2();
    }

    private void ResetCurrentWordIdxToZero()
    {
        _currentWordIdx = 0;
    }

    public void OnPreviousButtonClicked()
    {
        DecrementWordIdx();
        ResetUiAndSetPhaseTo2();
    }

    public void OnNextButtonClicked()
    {
        IncrementWordIdx();
        ResetUiAndSetPhaseTo2();
    }

    public void OnFlipToggleClicked()
    {
        _flipped = FlipMeaningToggle.isOn;
        ResetUiAndSetPhaseTo2();
    }

    public void OnShuffleButtonClieked()
    {
        UpdateCurrentWordList(_currentWordList.OrderBy(a => Guid.NewGuid()).ToList());
        ResetUiAndSetPhaseTo2();
    }

    public void OnMeaningToggleTapped()
    {
        _showMeaningOn = MeaningToggle.isOn;
        Debug.Log($"meaning toggle : {MeaningToggle.isOn}");
        ResetUiAndSetPhaseTo2();
    }

    private void ResetUiAndSetPhaseTo2()
    {

        Word currentWord = CurrentWord();
        string firstPhaseStr = _flipped ? currentWord._meaning : currentWord._spell;
        int firstPhaseFontSize = _flipped ? _flashMeaningFontSize : _flashWordFontSize;

        _flashTime = 0.0f;
        _currentPhaseIdx = 1;
        UpdateNoTextUi();
        UpdateWordUi(firstPhaseStr, firstPhaseFontSize);
        UpdateStudiedButtonUi();
    }

    public void OnWordJumpSliderValueChanged()
    {
        _currentWordIdx = (int)WordJumpSlider.value;
        /*
         * 以下でリセットしてしまうと、永遠にphase2になってしまう。
         * 次のインデックスに自動で進むたびに呼ばれてしまうため
         * 
         * ResetUiAndSetPhaseTo2();
         */
    }
    private void UpdateCurrentWordList(List<Word> newList)
    {
        _currentWordList = newList;
    }


    public void OnFixedSpeedToggleTapped()
    {
        _fixedSpeedOn = FixedSpeedToggle.isOn;
    }

    private void InitializeFlashSpeedSlider()
    {
        SpeedControlSlider.wholeNumbers = true;

        int minIdx = 0;
        int maxIdx = 1;
        SpeedControlSlider.minValue = _flashSpeedSliderMinAndMax[minIdx];
        SpeedControlSlider.maxValue = _flashSpeedSliderMinAndMax[maxIdx];

        UpdateFlashTime();
    }

    public void OnSpeedControlSliderValueChanged()
    {
        UpdateFlashTime();
    }

    private void UpdateFlashTime()
    {
        float coff = (SpeedControlSlider.value - SpeedControlSlider.minValue) / (SpeedControlSlider.maxValue - SpeedControlSlider.minValue);

        int minIdx = 0;
        int maxIdx = 1;

        float newFixedSpeed = coff * (_fixedSpeedMinAndMax[maxIdx] - _fixedSpeedMinAndMax[minIdx]) + _fixedSpeedMinAndMax[minIdx];
        float newVariableSpeed = coff * (_variableSpeedMinAndMax[maxIdx] - _variableSpeedMinAndMax[minIdx]) + _variableSpeedMinAndMax[minIdx];

        _fixedFlashIntervalTime = (float)Math.Round(newFixedSpeed, 1, MidpointRounding.AwayFromZero);
        _variableFlashIntervalCoefficient = (float)Math.Round(newVariableSpeed, 1, MidpointRounding.AwayFromZero);

    }

    private void DrawTextureOnStart()
    {
        var texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, Color.white);
        texture.Apply();
        _texture = texture;
    }

    private void OnGUI()
    {
        if (_flashRunning)
        {
            ProceedLoopGui();
        }
    }

    private void ProceedLoopGui()
    {
        float thresholdTime = _fixedSpeedOn ? _fixedFlashIntervalTime : CalculateVariableWordTime();

        float uiHeight = FlashWordButton.GetComponent<RectTransform>().rect.height;
        float screenHeight = Screen.height;

        Vector3 wordPos = FlashWordButton.transform.position;
        Vector3 wordLocalPos = FlashWordButton.transform.localPosition;
        Vector3 shuffleBtnPos = ShuffleButton.transform.position;

        float currentProgress = _flashTime < thresholdTime ? _flashTime : thresholdTime;

        if (IsCurrentPhase2() || !_showMeaningOn)
        {
            DrawProgressBar(480, 20, wordPos.x, screenHeight - wordPos.y + (uiHeight / 2), thresholdTime, currentProgress, this._progressBarPhase1FillColor, this._progressBarFrameColor);
        }
        else if (IsCurrentPhase1())
        {
            DrawProgressBar(480, 20, wordPos.x, screenHeight - wordPos.y + (uiHeight / 2), thresholdTime, thresholdTime, this._progressBarPhase1FillColor, this._progressBarFrameColor);
            DrawProgressBar(480, 10, wordPos.x, screenHeight - wordPos.y + (uiHeight / 2), thresholdTime, currentProgress, this._progressBarPhase2FillColor, this._progressBarFrameColor);
        }

        DebugText.text = $"pos: {wordPos.x:0000.00},{wordPos.y:0000.00}\npos: {wordLocalPos.x:0000.00},{wordLocalPos.y:0000.00}\npos: {shuffleBtnPos.x:0000.00},{shuffleBtnPos.y:0000.00}";

    }

    private void DrawProgressBar(float width, float height, float px, float py, float maxProgress, float currentProgress, Color barFillColor, Color barFrameColor)
    {
        float x = px - (width / 2);
        float y = py;

        var filledRect = new Rect(x, y, width / (maxProgress) * currentProgress, height);
        var emptyRect = new Rect(x, y, width, height);

        GUI.DrawTexture(filledRect, _texture, ScaleMode.StretchToFill, true, 0, barFillColor, 0, 0);
        GUI.DrawTexture(emptyRect, _texture, ScaleMode.StretchToFill, true, 0, barFrameColor, 3, 0);
    }


}
