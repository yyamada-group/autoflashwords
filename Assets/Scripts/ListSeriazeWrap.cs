using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ListSerialilzeWrap 
{
    public Word[] list;

    public ListSerialilzeWrap(List<Word> list)
    {
        this.list = list.ToArray();
    }
}
